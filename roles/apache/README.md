# Ansible Role: Apache

An Ansible role that install [Apache WebServer](https://httpd.apache.org/) on CentOS 7. It will configure the Apache WebServer and make it secure by default. It is creating `vhosts.conf`, `ssl.conf` and `httpd.conf` configuration files. It also creates selfsigned SSL Certificates in case we want to have HTTPS connections.

## Requirements

None.

## Role Variables


The available variables exist in the `defaults/main.yml` file

    # We can set the below variables to true/false depending on our needs.
    vhost_security_enabled: true
    apache_security_enabled: true
    apache_ssl_enabled: true
    ssl_security_enabled: true

Each variable is by default `true` and we recommend to use the default value. `vhost_security_enabled` is used to set basic security configuration for our apache virtual hosts. `apache_security_enabled` is used to set global security settings for the Apache WebServer. `apache_ssl_enabled` is used to define if we want to have HTTPS connections and if set to true it will create SSL Certificates as well. `ssl_security_enabled` is a variable that enables or disables the extra security configuration we set for SSL.


    http_port: 80
    https_port: 443
    web_root_path: /var/www/html
    server_name: www.example.com

We can edit basic configurations like `http_port`, `https_port` and `web_root_path` for our vhosts. Furthermore we can set the `server_name` of our virtual host.


    key_name: selfsigned

We can even set the name for our `.key` and `.crt` files that we are creating. 


## Dependencies


In order to create our SSL Certificates we are using the `openssl_privatekey`, `openssl_csr` and `openssl_certificate` modules. They need python 3 to work and we install alongside python 3 the required `python3-pip`, `pytho3-setuptools`, `libselinux-python3`. We also use the `ansible_python_interpreter: /usr/bin/python3` to set the default python for this module because the default could be python 2.7 or anything else. The whole process can be found in the `tasks/ssl_cert.yml`.


## Example Playbook


The example below is a basic example where we don't set any variables 

    - hosts: servers
      become: true
      roles:
        - apache


We will now demonstrate an example where we set the variable `apache_ssl_enabled: false` (the default is the true value as we already said.

    - hosts: servers
      become: true
      vars:
        apache_ssl_enabled: false
      roles:
        - apache


## License


BSD

## Author Information


This role was created in 2021.
