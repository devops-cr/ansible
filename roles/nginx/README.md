
# Ansible Role: NginX

An ansible role that installs [NginX WebServer](https://www.nginx.com/) on CentOS 7. It will configure the NginX WebServer and make it secure by default. We are creating 4 different files `nginx.conf`, `defaults.conf`, `security.conf` and `ssl.conf`. It can also create the desired SSL Certificate for our HTTPS connections.

## Requirements

None.

## Role Variables


The available variables exist in the `defaults/main.yml` file

    # We can set the below variables to true/false depending on our needs.
    nginx_security_enabled: true
    nginx_php_site_enabled: true
    nginx_ssl_enabled: true
    ssl_secure_options_enabled: true

Each variable is by default `true` and we recommend to use the default value. `nginx_security_enabled` is used to set basic security configuration for our NginX WebServer. `nginx_php_site_enabled` is used to set if we have a site that is running php or not because NginX need some extra configuration for NginX sites. `nginx_ssl_enabled` is used to define if we want to have HTTPS connections and if set to true it will create SSL Certificates as well. `ssl_secure_options_enabled` is a variable that enables or disables the extra security configuration we set for SSL.


    http_port: 80
    https_port: 443
    web_root_path: /var/www/html
    server_name: www.example.com

We can edit basic configurations like `http_port`, `https_port` and `web_root_path` for our vhosts. Furthermore we can set the `server_name` of our virtual host.

    key_name: selfsigned

We can even set the name for our `.key` and `.crt` files that we are creating.

We have set the variables `nginx_security_configuration` and `ssl_secure_configuration` with values about security for our sites. It is recommended not to change them unless we know what we want to add or delete. 


## Dependencies


In order to create our SSL Certificates we are using the `openssl_privatekey`, `openssl_csr` and `openssl_certificate` modules. They need python 3 to work and we install alongside python 3 the required `python3-pip`, `python3-setuptools`, `libselinux-python3`. We also use the `ansible_python_interpreter: /usr/bin/python3` to set the default python for this module because the default could be python 2.7 or anything else. The whole process can be found in the `tasks/ssl_cert.yml`.


## Example Playbook


The example below is a basic example where we don't set any variables

    - hosts: servers
      become: true
      roles:
        - nginx

We will now demostrate an example while we set the variable `apache_ssl_enabled: false` (the default is the true value as we already said.

    - hosts: servers
      become: true
      vars:
        nginx_ssl_enabled: false
      roles:
        - nginx


## License


BSD

## Author Information


This role was created in 2021.

