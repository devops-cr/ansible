Role Name
=========

This is a role where we are creating the structure for two projects that are connected to each other.
The first project is the Nagios Server project where we are creating a Server that can monitor itself and other clients installing 

- **Nagios Core v4.4.6**
- **Nagios Plugins v 2.3.3**
- **Nagios NRPE v 4.0.3**

Every package of the above is the latest as of the date this README.md is being written.

The second project is the Nagios Clients where we are creating several clients with the needed plugins and packages to be monitored from the Server. Especially we are installing 

- **Nagios Plugins v 2.3.3**
- **Nagios NRPE (Nagios Remote Plugin Executor) v4.0.3**

The images are being built with the multi-stage builds functionality that docker provides.

Let's explain the project and how it is created. As we already said there is a Nagios Server who by default can monitor itself with the `files/var/local/docker/nagios/server_image/opt/nagios/etc/objects/localhost.cfg` file. We can define our host and the services that we want to check there. 

We will use this file to create a template and create our Nagios Clients configuration file. This file is being located in `templates/var/local/docker/nagios/server_image/opt/nagios/etc/servers/template.j2`. We can create that way multiple clients that our Nagios Server can monitor. We have set up our template to monitor certain services on each client. These are 

- PING
- Free Space
- Current Users
- Total Processes
- System Load
- Swap Usage
- SSH
- HTTP
- IMAP

Some of these services may not be able to be resolved because we may don't have SSH available in our clients or an HTTP Server. 

We can add even more commands if we edit this file `(templates/var/local/docker/nagios/server_image/opt/nagios/etc/objects/servers/template.j2)` and add follow the same pattern another service. We most likely have to create another service and change the `check_command`. So an example could be 

    define service {
      
      use			local-service
      host_name			{{ item }}
      service_description 	"Add a Description Here"
      check_command		check_nrpe!check_uptime

    }

At this moment the server a new command that he can check on the clients but the clients can't "understand" this new command. So we have to edit in the clients section 

We have another edit to make to make our configuration work. We have to edit the file `files/var/local/docker/nagios/client_image/opt/nagios/etc/nrpe.cfg` find the command section that is uncommented (line 300) and add the command. That way when the client sees the command it will be able to know what to do with that command. Following the example above we will add the line below

    command[check_uptime]=/opt/nagios/libexec/check_uptime

We must be very careful and make sure that the second part of the `check_command` (`check_uptime`) is identical with the part inside the brackets `[]`. And then the path that this command is being located. Every command based on our setup is being located in the `/opt/nagios/libexec/` directory.




## Usage
To use this ansible role you just have to define the client names in the `defaults/main.yml` file. We can define them:

    clients:
      - client1
      - test_client
      - anothe_test_client

We can add as many we want and the role will create these clients. We have to be careful and not create two clients with the same name. 

To access the nagios page we simply have to type in our browser `http://NAGIOS_SERVER_IP_ADDRESS/nagios` and then we will be prompted to add a username and a password. We have set these to 

Username: `nagiosadmin`
Password: `nagios`

Requirements
------------

None.

Role Variables
--------------

The only variable that we have to set is the `clients` variable to set the clients that we need to monitor

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         -  nagios

License
-------

BSD

Author Information
------------------

This role was created in 2021
