# Elk

This role is created and it is based on the [deviantony/docker-elk](https://github.com/deviantony/docker-elk) repo. We are using only **elasticsearch** and **kibana**. We are not using logstash to parse our logs. We are also enabling by default ssl communications for kibana instance.

## Requirements

- Docker
- docker-compose

## Role Variables

There is only one possible variable that we have to set. This is the `elk_version` variable. 
We can find all the possible versions from the [docker elasticsearch](https://www.docker.elastic.co/). We only need which is usually in this format `XX.XX.XX` (e.g. `7.13.2`)

## Dependencies

None.

## Example Playbook

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - role: elk
           elk_version: 7.13.2

## Author Information

This role was created in 2021
