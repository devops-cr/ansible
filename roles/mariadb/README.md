# Ansible Role: MariaDB

This Ansible role is being used to install MariaDB client and MariaDB server with the latest version on a CentOS 7 machine. We use a `my_secure_installation` task which is a way to execute my_secure_installation that MariaDB and MySQL have as an option. We create a my.cnf file and finaly if we want to we can create a local and a remote user with different privileges.

We also create password for the root user in order to be able to log in from localhost and the loopback address.

## Requirements

None.


## Role Variables

The variables structure is different from the usual one.

    mariadb
    ├── defaults
    │   ├── main
    │   │   ├─  main.yml
    │   │   └── passowrd.yml


In the `main.yml` file we have our variables that we might want to change and in the password file we have encrypted the passwords we are using. 

Let's explain a bit more our password variables

    root_password: "{{ vault_root_password }}"
    local_password: "{{ vault_local_password }}"
    remote_password: "{{ vault_remote_password }}"

As we can understand we are getting the vault password from the `defaults/main/password.yml` file. If for example we want to change a password we should decrypt our file and change it from there.


    mariadb_version: "10.5"
    mariadb_port: 3306

The `mariadb_version` is a value that is being used to determine which MariaDB version we would like to install from the repos. At this moment 10.5 is the latest version and that's the one we are using. If for any reason we would like to install an older version we can change this value to 10.4, 10.3, etc. `mariadb_port` is the default port that MariaDB is using. We can change this value to change the MariaDB default port if we want to.


    mariadb_local_user_enabled: true
    mariadb_local_database: local_database
    mariadb_local_user_name: local_user

This is a set of values where we are creating a local user with all the privileges on `local_database`. Local user has all the privileges on the `local_database`. The default value for the variable `mariadb_local_user_enabled` is `true` which means that we are creating the local user and the local database by default.
 
    mariadb_remote_user_enabled: false
    mariadb_remote_database: remote_database
    mariadb_remote_user_name: remote_user
    mariadb_remote_host: "192.168.1.%"

This set of values exists so we can enable or not the remote user. Remote user has **INSERT, SELECT, UPDATE, DELETE and CREATE TEMPORARY TABLES** as privileges on `remote_database` only. The default value for the variable `mariadb_local_user_enabled` is `false` which means that we are not creating the remote user and the remote database by default. We also set a variable to determine the remote_user's host. We set it as `192.168.1.%` which means that the host is the whole network 192.168.1.0 but it is recommended to change that value as you might want another network or even a single IP Address for the remote user.


    remote_addresses: 0.0.0.0
    mariadb_max_connections: 30
    mariadb_innodb_buffer_pool_size: 128M

`remote_addresses` is used to set the remote addresses that MariaDB is listening to. We can change this value to a single address but it is not recommended. `mariadb_max_connections` is set to 30 when the default value is 151. It is recommended to change that value depending on the needs and the capabilities of the system. `mariadb_innodb_buffer_pool_size` is set to 128M which is the default but it is recommended to change that value depending on the RAM of your system. Usualy MariaDB recommends that this value should be 70%-80% of the systems RAM based on this [Official Documentation](https://mariadb.com/kb/en/mariadb-memory-allocation/).



## Dependencies

None.


## Example Playbook


A playbook exanple should look like the following one if we don't want to change the values of the variables

    - hosts: servers
      roles:
        - mariadb


If we would like to change the variables for this roles. Let's say for example the default port that mariadb is listening to we could use the following example

    - hosts: servers
      vars:
        mariadb_port: 8080
      roles:
        - mariadb

## License


BSD

## Author Information


This roles was created in 2021.
