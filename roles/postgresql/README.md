# Ansible Role: PostgreSQL

In this Ansible role we are installing a certain PostgreSQL version (11). We initialize and configure our `pg_hba.conf` file with our desired users. We also create a remote and local database and a remote and local role in our database. The version we are installing is `postgresql-11` but we can change that value to any Postgres version we want.


## Requirements
None.


## Role Variables


The Ansible variables that are listed below exists in the `default/main.yml`.


    postgres_version: 11

This is the value that we can change to install a different Postgres version. Available versions at this moment are `9.5`, `9.6`, `10`, `11`, `12` and `13`.


    pg_default_port: 5432

This is a variable that it configures the default listening port for Postgres and opens the equivalent TCP port.


    postgresql_pg_hba_default:
     - { type: local, database: all, role: "{{ pg_admin_user }}", address: "", method: "{{ pg_default_auth_method }}", comment: '"local" is for Unix domain socket connections only' }
     - { type: host, database: all, role: all, address: "127.0.0.1/32", method: "{{ pg_default_ipv4_method }}", comment: 'IPv4 local connections:' }
     - { type: host, database: all, role: all, address: "::1/128", method: "{{ pg_default_ipv6_method }}", comment: 'IPv6 local connections:' }

This is a set of variables that it is being used inside a loop in order to create the users that we want to have access. The loop is happening in the `templates/var/lib/pgsql/11/data/pg_hba.conf.j2`. With this configuration we are creating 3 user connections
- local connection for `postgres` user with trust connection method (which means it doesn't need password
- remote connection for IPv4 on the localhost address where we require password
- remote connection for IPv6 on the localhost address where we require password

# Specify custom rules for pg_hba.conf. Specify here all necessary pg_hba rules.
    postgresql_pg_hba_custom:
     - { type: host, database: replication, role: "{{ pg_admin_user }}", address: "127.0.0.1/32", method: "{{ pg_default_ipv4_method }}", comment: '' }

This is the basic configuration we are creating no matter what. But we have inserted some triggers where we can create a local user and a remote user if we want to. 

    postgres_local_user_enabled: true
    postgres_local_database: local_database
    postgres_local_user_name: local
    postgres_local_auth_method: "md5"

This is a set of variables that we can use to create a `local` user and connect it later on with the `local_database`. By default this value is set to true. The `md5` is the way that this user will be connected to our database.

    postgres_remote_user_enabled: false
    postgres_remote_database: remote_database
    postgres_remote_user_name: remote
    postgres_remote_host: "192.168.1.0/24"
    postgres_remote_auth_method: "md5"

This is a set of variables that we use to create a `remote` user and connect it with the `remote_database`. By default the remote host's trigger is set to `false`. The remote user can connect from the internal network only `192.168.1.0` and it is using password authentication.

We have to set these values to the `pg_hba.conf` file and that is why we added two more variables
    postgres_local_pg_hba:
      - { type: local, database: "{{ postgres_local_database }}", role: "{{ postgres_local_user_name }}", address: "", method: "{{ postgres_local_auth_method }}", comment: "local connection for local user only on local database" }

It is the same format as the one above but in the template 'var/lib/pgsql/11/data/pg_hba.conf' we have an `if` trigger to create this entry depending if we want to create the local user or not.

    postgres_remote_pg_hba:
      - { type: host, database: "{{ postgres_remote_database }}", role: "{{ postgres_remote_user_name }}", address: "{{ postgres_remote_host }}", method: "{{ postgres_remote_auth_method }}", comment: "remote connection for remote user only on remote database from the remote host we set" }


## Dependencies

psycopg2 is a dependency for this role. If not installed we won't be able to manipulate and create databases and users with ansible in postgres.


## Example Playbook


A simple example on the way that this role should be run is the one below.

    - hosts: servers
      roles:
         - postgres

If we want to set variables for that role we can use it like the example below

    - host: server
      vars:
        pg_default_port: 8080
      roles:
        - postgres

## License


BSD

## Author Information


This role was created in 2021.
