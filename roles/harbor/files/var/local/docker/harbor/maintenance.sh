#!/bin/sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
RED="\e[31m"
GREEN="\e[32m"
RESET="\e[0m"

usage(){

	echo -e "

	This script is being used to backup harbor registry and import
	the backed up files that we are creating. 

	${GREEN}Usage:${RESET}

	${GREEN} $0 backup ${RESET}
	  This command is being used to back up our harbor registry

	${GREEN} $0 import FILE_PATH ${RESET}
	  This command is being used to import our already backed up
	  files. FILE_PATH can be either a relative or an absolute path to the 
	  'tar.gz' file

	"

}

backup(){


	# The backup function is based on the official documentation 
	# which we can find here. 
	# https://goharbor.io/docs/2.2.0/administration/upgrade/
	echo "
	Stopping running containers "
	docker-compose down

	BACKUP_DIR=backup-$(date +%d-%m-%y)
	mkdir -p /${BACKUP_DIR}/harbor
	mkdir -p /${BACKUP_DIR}/data
	echo "
	Backing up harbor data"

	cp -ra /data/database /${BACKUP_DIR}/data
	cp -ra /data/cert /${BACKUP_DIR}/data
	cp -ra ${DIR}/* /${BACKUP_DIR}/harbor
	mkdir -p ${DIR}/backup
	tar cfz ${DIR}/backup/${BACKUP_DIR}.tar.gz /${BACKUP_DIR}
	echo "
	Back up is completed ! 
	"
	rm -rf /${BACKUP_DIR}

}

import(){


	# The import function is based on the official documentation
	# which we can find here 
	# https://goharbor.io/docs/2.2.0/administration/upgrade/roll-back-upgrade/
	IMPORT_FILE=$2

	if [ -z "$IMPORT_FILE" ]; then
		echo -e "
		${RED} Error: ${RESET} Not enough arguments"
		usage
		exit 1
	fi	
	if [ "$#" -ne 2 ]; then
		echo -e "
		${RED} Error: ${RESET} Too many arguments"
		usage
		exit 1
	fi
	if [ ! -f "$IMPORT_FILE" ]; then
		echo -e "
		${RED} Error: ${RESET} The file does not exist "
		usage
		exit 1
	fi

	echo "
	Stopping running containers "
	docker-compose down

	echo "
	Importing backup file
	"
	tar xfz ${IMPORT_FILE} -C /tmp
	yes | cp -ra /tmp/backup-*/data/. /data/
	yes | cp -ra /tmp/backup-*/harbor/. ${DIR}
	echo "
	Importing is completed !
	"
	rm -rf /tmp/backup-*

}

if [ -z "$1" ] || [ "$1" == "-h" ] || [ "$1" == "--help" ]; then

	usage
	exit 0

fi

COMMAND="$1"
case "$COMMAND" in
	backup)
		backup
		;;
	import)
		import "$@"
		;;
esac
