Role Name
=========

This role is installing harbor registry. It is dependent on the `docker` role. The harbor registry that is being installed is **v2.2.1**. Alongside with harbor we are installing trivy and notary. 

Trivy is a valnerability scanner that is used to find vulnerabilities in the images and notary is a tools that is being used to digital sign the images. 

We should mention the existance of the `maintenance.sh` script that it is being used to either **backup** or **import** our harbor registry. The script is being located in the `/var/local/docker/harbor/` path. We can find more information on how to use it if we execute it with the below flag.

    ./maintenance.sh --help 

Requirements
------------

`Docker` role is the only requirement to use harbor role. Harbor needs docker and docker-compose in order to be installed and run. That's why we need both roles to make harbor work.

We also need a `vault.yml` file to store our admin and database password to login into harbor.
The `vault.yml` file should look like the example below

    vault_admin_password=ENTER_PASSWORD_HERE
    vault_database_password=ENTER_PASSWORD_HERE


Role Variables
--------------

We have a variable called `harbor_release` which by default installs **harbor v2.2.1**.

Other usefull variables are `harbor_extras` where we type the tools that we need to install harbor with. We have selected as default tools `trivy` and `notary` but there are others as well. One example is `chartmuseum` that is being used in cubernetes clusters. 


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - harbor


License
-------

BSD

Author Information
------------------

This role was created in 2021 
