# Ansible Role: Docker

This is a role that install `docker-ce`, `docker-ce-cli`, `containerd.io` and `docker compose`.

Syslog is used as the default way to log the output of the docker containers. The docker logs are being sent to `/var/log/docker/combined.log`.

## Requirements

None.

## Role Variables

None.

## Dependencies

None.

## Example Playbook

    - hosts: servers
      roles:
      - role: docker


## License

BSD
