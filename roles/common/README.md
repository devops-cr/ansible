common
=========

This is a common role which installs basic packages and configures the
environment for the root user. The role covers RedHat, Debian and Ubuntu
systems. Some of the packages that are being installed are:

- vim
- tmux
- nnn
- git
- htop

The configuration for these packages comes alongside.

Requirements
------------

None.

Role Variables
--------------

None.

Dependencies
------------

None.

Example Playbook
----------------

    - hosts: servers
      roles:
      - role: common

License
-------

BSD
