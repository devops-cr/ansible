# check_mk

This is a check_mk role where we can create a check_mk server and/or a check_mk
client. We need to declare which one we want between those two. Their difference
is that if we install check_mk for clients we will not install the whole
check_mk package but only the required files that we need in order to check our
client. The check_mk version that we are installing is **v1.6.0p24**


## Requirements

- CentOS 7

## Role Variables

We are specifying some default variables. For example:

    monitor_site: check_mk
    check_mk_server_enabled: false
    check_mk_client_enabled: false

These are mostly the variables that we would like to set and configure our
check_mk instance. There are other variables that we can use to set **ssl** and
**ssl_security** options that we have set them to true by default. 

Finally the last variable that we have to set is the `check_mk_password`. If we
go to `defaults/main/main.yml` we will see that this variable is being drawn
from another variable called `vault_check_mk_password`. This variable is being
set in a separate file that it is encrypted. To set the variable the easiest
option is to follow the below steps:

    vim defaults/main/check_mk_password.yml

Type in the file 

    vault_check_mk_password: "ADD_A_PASSWORD_HERE"

and we are done.

Example Playbook
----------------

For check_mk servers:

    - hosts: servers
      roles:
         - { role: check_mk, check_mk_server_enabled: true }

For check_mk_clients:

    - hosts: servers
      roles:
         - { role: check_mk, check_mk_client_enabled: true }

Author Information
------------------

This role was created in 2021.
